/* ************************************************************************** */
/*                                                                            */
/*                                                                      / )   */
/*   main.cc                                            (\__/)         ( (    */
/*                                                      )    (          ) )   */
/*   By: lejulien <leo.julien.42@gmail.com>           ={      }=       / /    */
/*                                                      )     `-------/ /     */
/*   Created: 2023/01/24 01:26:57 by lejulien          (               /      */
/*   Updated: 2023/01/28 09:56:24 by lejulien           \              |      */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pix.hpp"

// TODO : put image data inside another image

int		main(int ac, char **av) {
	// Create image, args: <width> <height>
	pix::Image img(500, 400);

	// Create image from file, arg: <path>
	pix::Image flag("./assets/out.ppm");
	
	// Clear image with color, arg: <color>
	img.fillImg({200, 50, 200}); 
	
	// draw rectangle, args: <pos> <size> <color>
	img.drawRect({2, 2}, {300, 150}, {0, 0, 255});
	img.drawRect({10, 10}, {16, 16}, {150, 10, 150});

	// draw circle, args: <pos> <radius> <color>
	img.drawCircle({250, 200}, 180, {0, 0, 0});

	// print text, args: <str> <pos> <color>
	img.print("42421234567890", {20, 100}, {0, 255, 0});
	img.print("101", {450, 300}, {255, 0, 0});
	img.print("PiX = 42", {350, 50}, {0, 255, 255});
	img.print("hello world !", {210, 320}, {200, 200, 255});

	// draw line, args: <pos1> <pos2> <color>
	img.line({10, 10}, {490, 390}, {0, 255, 0});

	// save image, arg: <filepath>
	img.saveAs("out.ppm");
	return 0;
}
